use std::iter::FromIterator;
use std::sync::{
    mpsc::{self, Sender},
    Arc, RwLock,
};
use std::thread::JoinHandle;
use std::{collections::HashSet, time::Duration};

use etherparse::{InternetSlice, SlicedPacket};
use regex::Regex;
use windivert::{CloseAction, WinDivert, WinDivertFlags, WinDivertLayer, WinDivertParsedSlice};

use super::{Filter, ListAction, Type, MTU};

// TODO: Logging

const PACKET_COUNT: usize = 255;
const BUFFER_SIZE: usize = MTU * PACKET_COUNT;

#[derive(Debug)]
pub struct OSFilter {
    filter: String,
    regex: Option<Regex>,
    whitelist: Option<Vec<u32>>,
    blacklist: Option<Vec<u32>>,
    filter_type: Type,
    lock: Arc<RwLock<bool>>,
    thread: Option<JoinHandle<Vec<u32>>>,
    sender: Option<Sender<ListAction>>,
}

impl OSFilter {
    pub fn new(ports: String, regex: Option<Regex>) -> Self {
        let ports_filter = ports
            .split(",")
            .map(|p| p.trim())
            .map(|s| {
                if !s.contains('-') {
                    //format!("(udp.SrcPort == {0} or udp.DstPort == {0})", s)
                    format!("(udp.DstPort == {0})", s)
                } else {
                    let mut ports = s.split('-').collect::<Vec<_>>();
                    let end = ports.pop().expect("Unreachable, format tested before");
                    let start = ports.pop().expect("Unreachable, format tested before");
                    //format!("(udp.SrcPort >= {0} and udp.SrcPort <= {1}) or (udp.DstPort >= {0} and udp.DstPort <= {1})", start, end)
                    format!("(udp.DstPort >= {0} and udp.DstPort <= {1})", start, end)
                }
            })
            .collect::<Vec<_>>()
            .join(" or ");
        let filter = format!("ip and ({})", ports_filter);

        Self {
            filter,
            regex,
            whitelist: None,
            blacklist: None,
            filter_type: Type::Solo,
            lock: Arc::new(RwLock::new(false)),
            thread: None,
            sender: None,
        }
    }

    fn set_stop(&mut self, value: bool) -> Result<(), ()> {
        if let Ok(mut writer) = self.lock.write() {
            *writer = value;
            Ok(())
        } else {
            Err(())
        }
    }
}

impl Filter for OSFilter {
    fn start(&mut self) {
        if self.thread.is_none() {
            let _ = self.set_stop(false);
            let (sender, receiver) = mpsc::channel();
            self.sender = Some(sender);
            let exit = Arc::clone(&self.lock);
            let mut dynamiclist: HashSet<u32> = HashSet::new();
            let whitelist: HashSet<u32> = if let Some(ips) = self.whitelist.take() {
                HashSet::from_iter(ips.into_iter())
            } else {
                HashSet::new()
            };
            let blacklist: HashSet<u32> = if let Some(ips) = self.blacklist.take() {
                HashSet::from_iter(ips.into_iter())
            } else {
                HashSet::new()
            };
            let filter_type = Arc::new(self.filter_type);
            let filter = self.filter.clone();
            let ip_filter = self.regex.clone().unwrap_or(Regex::new(r#".^"#).unwrap());
            let thread = std::thread::Builder::new().name("Blocker".into());
            self.thread = Some(
                thread
                .spawn(move || {
                    let divert = WinDivert::new(
                        &filter,
                        WinDivertLayer::Network,
                        0,
                        WinDivertFlags::default(),
                    );
                    if let Err(e) = &divert {
                        panic!("Unable to start WinDivert: {:?}", e)
                    }
                    let mut divert = divert.expect("Unreachable");

                    let mut collected: HashSet<u32> = HashSet::new();

                    loop {
                        if let Ok(Some(data)) =
                        divert.recv_ex_wait(BUFFER_SIZE, 100, PACKET_COUNT)
                        {
                            let data = data
                            .into_iter()
                            .filter(|item| {
                                if let WinDivertParsedSlice::Network { addr, data } = item.parse_slice() {
                                    let packet = SlicedPacket::from_ip(data)
                                    .expect("Not possible to get invalid ip packets from windivert");
                                    if let Some(InternetSlice::Ipv4(header, _)) = packet.ip {
                                        let str_src = header.source_addr().to_string();
                                        let src = header.source_addr().into();
                                        return match *filter_type {
                                            Type::Solo => false,
                                            Type::Whitelist => {
                                                [12, 18].contains(&header.payload_len())
                                                || [245, 261, 277, 293].contains(&header.payload_len())
                                                || whitelist.contains(&src)
                                                || dynamiclist.contains(&src)
                                                || ip_filter.is_match(&str_src)
                                            }
                                            Type::Blacklist => !blacklist.contains(&src) || addr.outbound(),
                                            Type::Both => {
                                                [12, 18].contains(&header.payload_len())
                                                || [245, 261, 277, 293].contains(&header.payload_len())
                                                || (!blacklist.contains(&src)
                                                    && (dynamiclist.contains(&src)
                                                    || ip_filter.is_match(&str_src)
                                                    || whitelist.contains(&src))
                                                )
                                            }
                                            Type::Collect => {
                                                collected.insert(src);
                                                true
                                            }
                                        }
                                    }
                                }
                                // Keep any packet that failed the previous match as a fallback.
                                // This return value should never be reached.
                                true
                            }).collect();
                            let _ = divert.send_ex(data);
                        }
                        // Check for ips to add/delete
                        if let Ok(action) = receiver.recv_timeout(Duration::from_millis(1)) {
                            match action {
                                ListAction::Add(values) => {
                                    dynamiclist.extend(values.into_iter())
                                }
                                ListAction::Del(values) => {
                                    for value in values {
                                        dynamiclist.remove(&value);
                                    }
                                }
                                ListAction::Set(values) => {
                                    dynamiclist.clear();
                                    dynamiclist.extend(values.into_iter());
                                }
                            };
                        }
                        // Check for exit condition
                        if let Ok(val) = exit.try_read() {
                            if *val {
                                break;
                            }
                        }
                    }
                    let _ = divert.close(CloseAction::Uninstall);
                    collected.into_iter().collect()
                })
                .unwrap(),
            )
        }
    }

    fn stop(&mut self) -> Option<Vec<u32>> {
        if self.thread.is_some() {
            if self.set_stop(true).is_ok() {
                if let Ok(addresses) = self.thread.take().unwrap().join() {
                    return Some(addresses);
                }
            }
        }
        None
    }

    fn add_ips(&mut self, values: Vec<u32>) {
        if let Some(s) = &self.sender {
            let _ = s.send(ListAction::Add(values));
        } else {
            match self.whitelist.as_mut() {
                Some(ips) => ips.extend(values.into_iter()),
                None => self.whitelist = Some(values),
            }
        }
    }

    fn del_ips(&mut self, values: Vec<u32>) {
        if let Some(s) = &self.sender {
            let _ = s.send(ListAction::Del(values));
        } else {
            match self.whitelist.as_mut() {
                Some(ips) => ips.retain(|ip| values.contains(ip)),
                None => {}
            }
        }
    }

    fn set_ips(&mut self, values: Vec<u32>) {
        if let Some(s) = &self.sender {
            let _ = s.send(ListAction::Set(values));
        } else {
        }
    }

    fn clean() {
        let _ = WinDivert::uninstall();
    }

    fn set_list_type(&mut self, list_type: Type) {
        self.filter_type = list_type
    }

    fn set_blacklist(&mut self, blacklist: Vec<u32>) {
        if let None = self.thread {
            self.blacklist = Some(blacklist)
        }
    }
}

impl Drop for OSFilter {
    fn drop(&mut self) {
        if self.thread.is_some() {
            self.stop();
        }
    }
}
