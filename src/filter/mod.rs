#[cfg_attr(target_os = "windows", path = "windows.rs")]
mod os_filter;

use serde::{Deserialize, Serialize};

pub use os_filter::OSFilter;

const MTU: usize = 1500;

#[derive(Debug, Clone, Copy, Deserialize, Serialize)]
pub enum Type {
    Solo,
    Whitelist,
    Blacklist,
    Both,
    Collect,
}

impl std::default::Default for Type {
    fn default() -> Self {
        Self::Solo
    }
}

impl From<String> for Type {
    fn from(value: String) -> Self {
        if value.eq("whitelist") {
            Type::Whitelist
        } else if value.eq("blacklist") {
            Type::Blacklist
        } else if value.eq("both") {
            Type::Both
        } else {
            Type::Solo
        }
    }
}

#[derive(Debug)]
enum ListAction {
    Add(Vec<u32>),
    Del(Vec<u32>),
    Set(Vec<u32>),
}

pub trait Filter {
    fn start(&mut self);
    fn stop(&mut self) -> Option<Vec<u32>>;
    //fn collect(&mut self) -> Value;
    fn set_list_type(&mut self, list_type: Type);
    fn set_blacklist(&mut self, blacklist: Vec<u32>);
    fn add_ips(&mut self, values: Vec<u32>);
    fn del_ips(&mut self, values: Vec<u32>);
    fn set_ips(&mut self, values: Vec<u32>);
    fn clean();
}
