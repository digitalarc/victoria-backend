#[macro_use]
extern crate clap;
use clap::App;
use serde_json::Value;

mod filter;

use std::{io::Read, time::Duration};

use interprocess::local_socket::{LocalSocketListener, LocalSocketStream, ToLocalSocketName};
use regex::Regex;
use serde::{Deserialize, Serialize};

use filter::{Filter, OSFilter, Type};

#[derive(Deserialize, Debug, Serialize)]
#[serde(tag = "action")]
enum Action {
    Start {
        whitelist: Vec<u32>,
        blacklist: Vec<u32>,
        list_type: Type,
    },
    Stop,
    Add {
        ips: Vec<u32>,
    },
    Del {
        ips: Vec<u32>,
    },
    Set {
        ips: Vec<u32>,
    },
    Terminate,
}

#[derive(Deserialize, Debug, Serialize)]
enum List {
    Whitelist,
    Blacklist,
}

struct Server {
    listener: LocalSocketListener,
    filter: Option<OSFilter>,
    end: bool,
}

impl<'a> Server {
    fn new<A: ToLocalSocketName<'a>>(name: A, filter: Option<OSFilter>) -> Self {
        Self {
            listener: LocalSocketListener::bind(name).unwrap(),
            filter,
            end: false,
        }
    }

    fn handle_connection(&mut self, mut stream: LocalSocketStream) {
        let mut buffer = [0; 1024];

        while !self.end {
            if let Ok(len) = stream.read(&mut buffer) {
                let s = String::from_utf8_lossy(&buffer[..len]);
                let action = serde_json::from_str(s.trim());
                if let Ok(action) = action {
                    match action {
                        Action::Start {
                            whitelist,
                            blacklist,
                            list_type,
                        } => {
                            if self.filter.is_some() {
                                let f = self.filter.as_mut().unwrap();
                                f.set_list_type(list_type);
                                f.set_blacklist(blacklist);
                                f.add_ips(whitelist);
                                f.start();
                            }
                        }
                        Action::Stop => {
                            if self.filter.is_some() {
                                self.filter.as_mut().unwrap().stop();
                            }
                        }
                        Action::Add { ips } => {
                            if self.filter.is_some() {
                                self.filter.as_mut().unwrap().add_ips(ips);
                            }
                        }
                        Action::Del { ips } => {
                            if self.filter.is_some() {
                                self.filter.as_mut().unwrap().del_ips(ips);
                            }
                        }
                        Action::Set { ips } => {
                            if self.filter.is_some() {
                                self.filter.as_mut().unwrap().set_ips(ips);
                            }
                        }
                        Action::Terminate => {
                            self.end = true;
                        }
                    }
                }
            }
        }
    }

    fn start(&mut self) {
        let stream = self.listener.accept().unwrap();
        self.handle_connection(stream);
    }
}

impl Drop for Server {
    fn drop(&mut self) {
        if let Some(mut filter) = self.filter.take() {
            filter.stop();
        }
        OSFilter::clean();
    }
}

fn valid_ports(ports: &str) -> bool {
    // Check if all items are valid u16
    if ports
        .split(|c| c == ',' || c == '-')
        .all(|p| p.trim().parse::<u16>().is_ok())
    {
        // Check if range items are correct
        ports.split(',').filter(|&s| s.contains('-')).all(|range| {
            match range.split('-').collect::<Vec<_>>()[..] {
                [start, end] => {
                    let start: u16 = start.parse().expect("Unreachable, already tested");
                    let end: u16 = end.parse().expect("Unreachable, already tested");
                    start < end
                }
                _ => false,
            }
        })
    } else {
        false
    }
}

fn main() {
    let yaml = load_yaml!("cli.yml");
    let m = App::from_yaml(yaml).get_matches();

    // Handle the collect subcommand execution
    if let Some(collect_matches) = m.subcommand_matches("collect") {
        let ports = collect_matches
            .value_of("PORTS")
            .expect("Unreachable, prameter is required by cli.yml config");

        if !valid_ports(ports) {
            panic!("PORTS parameter has invalid format or values")
        }

        let mut filter = OSFilter::new(String::from(ports), None);
        filter.set_list_type(Type::Collect);
        filter.start();
        std::thread::sleep(Duration::from_secs(10));
        if let Some(addresses) = filter.stop() {
            println!("{}", Value::from(addresses).to_string())
        } else {
            println!("[]")
        }
    }
    // Normal ipc server execution
    if let (Some(ports), regex) = (m.value_of("PORTS"), m.value_of("REGEX")) {
        if !valid_ports(ports) {
            panic!("PORTS parameter has invalid format or values")
        }

        let ipc_name = m
            .value_of("IPC")
            .expect("Unreachable, parameter is required by cli.yml config");

        let regex =
            regex.map(|r| Regex::new(r).expect("Regex parameter must be a valid regex expression"));

        let mut server = Server::new(ipc_name, Some(OSFilter::new(String::from(ports), regex)));
        server.start();
    }
}
